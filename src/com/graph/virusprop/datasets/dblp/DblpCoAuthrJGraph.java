package com.graph.virusprop.datasets.dblp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.ext.*;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;
import com.graph.virusprop.jgraph.VertexStatusProvider;

public class DblpCoAuthrJGraph {

	public static void main(String[] args)
			throws TransformerConfigurationException, SAXException {
		String file = "dblp_coAuth.txt";

		UndirectedGraph<Vertex, DefaultEdge> powerlaw = createStringGraph();

		try {
			
			int times = 0;
			int timeval = 0;
			double heal_rate1 = 0.0;
			double heal_rate2 = 0.0;
			double inf_rate1 = 0.0;
			double inf_rate2 = 0.0;
			int degree = 0;

			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());

			System.out.print("Enter healing rate for profile 0 :");
			String input2 = br.readLine();
			heal_rate1 = Double.parseDouble(input2);

			System.out.print("Enter healing rate for profile 1 :");
			String input3 = br.readLine();
			heal_rate2 = Double.parseDouble(input3);

			System.out.print("Enter infection rate for profile 0 :");
			String input4 = br.readLine();
			inf_rate1 = Double.parseDouble(input4);

			System.out.print("Enter infection rate for profile 1 :");
			String input5 = br.readLine();
			inf_rate2 = Double.parseDouble(input5);

			System.out.print("Enter minimum degree of infection :");
			String input6 = br.readLine();
			degree = Integer.parseInt(input6);

			br.close();
			/*
			 * ===============graph creation===============
			 */
			BufferedReader br2 = new BufferedReader(new FileReader(file));

			String line = null;

			int count = 0;// number of nodes
			while ((line = br2.readLine()) != null) {

				String[] parts = line.split(" ");

				Vertex v1 = new Vertex();
				v1.setId(parts[0]);

				Vertex v2 = new Vertex();
				v2.setId(parts[1]);

				powerlaw.addVertex(v1);
				powerlaw.addVertex(v2);

				if (v1.getId().equals(v2.getId()) == false) {
					if ((powerlaw.containsEdge(v1, v2)) == false
							|| (powerlaw.containsEdge(v2, v1) == false)) {

						powerlaw.addEdge(v1, v2);
					}

				}

				count++;
				if ((count % 50000) == 0) {
					System.out.println("Count is " + count);
					System.out.println("Vetrices "
							+ powerlaw.vertexSet().size());
					System.out.println("Edges " + powerlaw.edgeSet().size());
				}

			}
			br2.close();

			Set<Vertex> vertices2 = powerlaw.vertexSet();
			//compute max degree in network
			int max_deg = 0;
			for (Vertex v : vertices2) {
				if (max_deg < powerlaw.degreeOf(v))
					max_deg = powerlaw.degreeOf(v);

			}
			System.out.println("Max degree is" + max_deg);

			int min_deg = max_deg;
			//compute min degree in network
			for (Vertex v : vertices2) {
				if (min_deg > powerlaw.degreeOf(v))
					min_deg = powerlaw.degreeOf(v);

			}
			System.out.println("Min degree is" + min_deg);
			System.out.println("Vetrices " + powerlaw.vertexSet().size());
			System.out.println("Edges " + powerlaw.edgeSet().size());

			int i = 0;
			int init_infect = 0;
			
			//Initial Infection process
			for (Vertex v : vertices2) {
				i++;

				if (i < vertices2.size() / 2) {
					if (powerlaw.edgesOf(v).size() >= degree) {
						//all nodes that have the desired degree, get infected
						v.setStatus(1);
						init_infect++;
					} else {
						v.setStatus(0);
					}
					v.setProfile(0); //equally divide the network in profiles 0 and 1
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
				} else {
					if (powerlaw.edgesOf(v).size() >= degree) {
						v.setStatus(1);
						init_infect++;
					} else {
						v.setStatus(0);
					}
					v.setProfile(1);
					v.setHeal_rate(heal_rate2);
					v.setInf_rate(inf_rate2);
				}

			}

			// produce .graphml file of initially infected nodes
			Writer writer = new FileWriter("powerlaw_init.graphml");
			GraphMLExporter<Vertex, DefaultEdge> exporter = new GraphMLExporter<Vertex, DefaultEdge>(
					new IntegerNameProvider<Vertex>(),
					new VertexStatusProvider<Vertex>(),
					new IntegerEdgeNameProvider<DefaultEdge>(), null);

			exporter.export(writer, powerlaw);

			File file2 = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(
					file2));
			System.setOut(printStream);
			System.out.println("Initially infected: " + init_infect);

			/*************************************** infection process *****************************************/
			Set<Vertex> vertices = powerlaw.vertexSet();// select in set the
														// nodes of the graph

			while (times <= timeval) {

				for (Vertex v : vertices) {
					v.setNew_status(0);
					v.setDelta(0);
				}

				
				for (Vertex v : vertices) {

					if (v.getStatus() == 1) { // infected node

						double dice = Math.random();

						if ((dice <= v.getHeal_rate())) {

							v.setDelta(1);

						}

						// we collect the neighbors for each vertex
						for (DefaultEdge e : powerlaw.edgesOf(v)) {

							Vertex vn = powerlaw.getEdgeTarget(e);
							if (vn == v) {
								vn = powerlaw.getEdgeSource(e);
							}

							dice = Math.random();
							if (dice <= vn.getInf_rate()) {
								vn.setNew_status(1);
							}

						}
					}// end of if

				}// end of for set iteration

				for (Vertex v : vertices) {
					if ((v.getStatus() == 0) && (v.getNew_status() == 1)) {
						v.setStatus(1);
						// System.out.println("Node "+v.getId()+" got infected");
					}

					else if ((v.getStatus() == 1) && (v.getNew_status() == 0)
							&& (v.getDelta() == 1)) {
						v.setStatus(0);
						// System.out.println("Node "+v.getId()+" got well");
					}
				}

				int sum = 0;
				int infect_0 = 0;
				int infect_1 = 0;
				for (Vertex v : vertices) {
					if ((v.getStatus() == 1) && (v.getProfile() == 0)) {
						infect_0++;
					}
					if ((v.getStatus() == 1) && (v.getProfile() == 1)) {
						infect_1++;
					}

				}
				sum = infect_0 + infect_1;

				System.out.println("\n");
				System.out.println("Round: " + times);
				System.out.println("the amount of infected nodes is " + sum);

				System.out.println("profile 0:" + infect_0);
				System.out.println("profile 1:" + infect_1);

				times++;
			}// end of while

			// produce .graphml file of finally infected nodes
			Writer writer2 = new FileWriter("powerlaw_fin.graphml");
			GraphMLExporter<Vertex, DefaultEdge> exporter2 = new GraphMLExporter<Vertex, DefaultEdge>(
					new IntegerNameProvider<Vertex>(),
					new VertexStatusProvider<Vertex>(),
					new IntegerEdgeNameProvider<DefaultEdge>(), null);

			exporter2.export(writer2, powerlaw);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}

}
