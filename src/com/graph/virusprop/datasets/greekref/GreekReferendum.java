package com.graph.virusprop.datasets.greekref;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.ext.IntegerEdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;
import com.graph.virusprop.jgraph.VertexStatusProvider;

public class GreekReferendum {
	public static void main(String[] args)
			throws TransformerConfigurationException, SAXException {
		String file = "friendsprofiles.txt";

		UndirectedGraph<Vertex, DefaultEdge> referendum = createStringGraph();

		try {
			int times = 0;
			int timeval = 0;
			double heal_rate1 = 0.0;
			double heal_rate2 = 0.0;
			double inf_rate1 = 0.0;
			double inf_rate2 = 0.0;

			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());

			System.out.print("Enter healing rate for profile Positive :");
			String input2 = br.readLine();
			heal_rate1 = Double.parseDouble(input2);

			System.out.print("Enter healing rate for profile Negative :");
			String input3 = br.readLine();
			heal_rate2 = Double.parseDouble(input3);

			System.out.print("Enter infection rate for profile Positive :");
			String input4 = br.readLine();
			inf_rate1 = Double.parseDouble(input4);

			System.out.print("Enter infection rate for profile Negative :");
			String input5 = br.readLine();
			inf_rate2 = Double.parseDouble(input5);

			br.close();
			/*
			 * =============================================graph
			 * creation=======================================================
			 */
			BufferedReader br2 = new BufferedReader(new FileReader(file));

			String line = null;

			int count = 0;// number of nodes
			while ((line = br2.readLine()) != null) {

				String[] parts = line.split("\t");
			
				Vertex v1 = new Vertex();
				v1.setId(parts[0]);
				v1.setProfile(Integer.parseInt(parts[2]));

				Vertex v2 = new Vertex();
				v2.setId(parts[1]);
				v2.setProfile(Integer.parseInt(parts[3]));

				referendum.addVertex(v1);
				referendum.addVertex(v2);
				if (v1.getId().equals(v2.getId()) == false) {
					if ((referendum.containsEdge(v1, v2)) == false
							|| (referendum.containsEdge(v2, v1) == false)) {

						referendum.addEdge(v1, v2);
					}

				}

				count++;
				if ((count % 50000) == 0) {
				System.out.println("======================================");
				System.out.println("Count is " + count);
				System.out.println("Total Vertices " + referendum.vertexSet().size());
				System.out.println("Total Edges " + referendum.edgeSet().size());
				System.out.println("======================================");
				}

			}
			br2.close();

			Set<Vertex> vertices2 = referendum.vertexSet();
			int max_deg1 = 1;
			int sec_max_deg1=0;
			int max_deg2 = 0;
			int max_deg=0;
			for (Vertex v : vertices2) {
				if(v.getProfile()==1){
					if (max_deg1 < referendum.degreeOf(v)){
						sec_max_deg1=max_deg1;
						max_deg1 = referendum.degreeOf(v);
					}else if(sec_max_deg1<referendum.degreeOf(v)&& referendum.degreeOf(v)!=max_deg1){
						sec_max_deg1=referendum.degreeOf(v);
					}
						
				}
				
				if(v.getProfile()==2){
					if (max_deg2 < referendum.degreeOf(v))
						max_deg2 = referendum.degreeOf(v);
				}
				

			}
			System.out.println("Max degree for profile 1 is " + max_deg1);
			System.out.println("Max degree for profile 2 is " + max_deg2);
			System.out.println("Second Max degree for profile 1 is " + sec_max_deg1);

			if(max_deg1>=max_deg2){
				max_deg=max_deg1;
			}
			else max_deg=max_deg2;
			
			int min_deg = max_deg;
			for (Vertex v : vertices2) {
				if (min_deg > referendum.degreeOf(v))
					min_deg = referendum.degreeOf(v);

			}
			System.out.println("Min degree is " + min_deg);

			int i = 0;
		
			int init_infect1 = 0;
			int init_infect2 = 0;
			for (Vertex v : vertices2) {
				i++;

		
					
						
						if(v.getProfile()==1 && referendum.edgesOf(v).size()>=950){
							v.setStatus(1);
							init_infect1++;
						}
						if(v.getProfile()==2 && referendum.edgesOf(v).size()>=950){
							v.setStatus(1);
							init_infect2++;
						}
						
						
					 else {
						v.setStatus(0);
					}

				

				if (v.getProfile() == 1) {
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
				} 
				else {
						if (v.getProfile() == 2) {
							v.setHeal_rate(heal_rate2);
							v.setInf_rate(inf_rate2);
						}

					}

			}

			// produce .graphml file of initially infected nodes
			Writer writer = new FileWriter("referendum_init.graphml");
			GraphMLExporter<Vertex, DefaultEdge> exporter = new GraphMLExporter<Vertex, DefaultEdge>(
					new IntegerNameProvider<Vertex>(),
					new VertexStatusProvider<Vertex>(),
					new IntegerEdgeNameProvider<DefaultEdge>(), null);

			exporter.export(writer, referendum);

			File file2 = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(
					file2));
			System.setOut(printStream);
			System.out.println("Initially infected profile positive: " + init_infect1);
			System.out.println("Initially infected profile negative: " + init_infect2);
			

			/*************************************** infection process *****************************************/
			// select in set the nodes of the graph
			Set<Vertex> vertices = referendum.vertexSet();
				
			while (times <= timeval) {

				for (Vertex v : vertices) {
					v.setNew_status(0);
					v.setDelta(0);
				}

				// for (int i = 0; i < N; i++) {
				for (Vertex v : vertices) {

					if (v.getStatus() == 1) { // infected node

						double dice = Math.random();

						if ((dice <= v.getHeal_rate())) {

							v.setDelta(1);

						}

						// we collect the neighbors for each vertex
						for (DefaultEdge e : referendum.edgesOf(v)) {

							Vertex vn = referendum.getEdgeTarget(e);
							if (vn == v) {
								vn = referendum.getEdgeSource(e);
							}

							dice = Math.random();
							if (dice <= vn.getInf_rate()) {
								vn.setNew_status(1);
							}

						}
					}// end of if

				}// end of for set iteration

				for (Vertex v : vertices) {
					if ((v.getStatus() == 0) && (v.getNew_status() == 1)) {
						v.setStatus(1);
						// System.out.println("Node "+v.getId()+" got infected");
					}

					else if ((v.getStatus() == 1) && (v.getNew_status() == 0)
							&& (v.getDelta() == 1)) {
						v.setStatus(0);
						// System.out.println("Node "+v.getId()+" got well");
					}
				}

				int sum = 0;
				int infect_0 = 0;
				int infect_1 = 0;
				for (Vertex v : vertices) {
					if ((v.getStatus() == 1) && (v.getProfile() == 1)) {
						infect_0++;
					}
					if ((v.getStatus() == 1) && (v.getProfile() == 2)) {
						infect_1++;
					}

				}
				sum = infect_0 + infect_1;

				System.out.println("\n");
				System.out.println("Round: " + times);
				System.out.println("the amount of infected nodes is " + sum);

				System.out.println("profile 1:" + infect_0);
				System.out.println("profile 2:" + infect_1);

				times++;
			}// end of while
		

			// produce .graphml file of finally infected nodes
			Writer writer2 = new FileWriter("referendum_fin.graphml");
			GraphMLExporter<Vertex, DefaultEdge> exporter2 = new GraphMLExporter<Vertex, DefaultEdge>(
					new IntegerNameProvider<Vertex>(),
					new VertexStatusProvider<Vertex>(),
					new IntegerEdgeNameProvider<DefaultEdge>(), null);

			exporter2.export(writer2, referendum);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}

}
