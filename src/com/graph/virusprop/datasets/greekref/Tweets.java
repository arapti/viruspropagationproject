package com.graph.virusprop.datasets.greekref;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.ext.*;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;
import com.graph.virusprop.jgraph.VertexStatusProvider;

public class Tweets {
	public static void main(String[] args) throws IOException {

		String file = "friendsfinal.txt";

		UndirectedGraph<Vertex, DefaultEdge> tweets = createStringGraph();
		BufferedReader br2 = new BufferedReader(new FileReader(file));

		try 
		{
			String line = null;

			// int count=0;//number of nodes
			while ((line = br2.readLine()) != null) 
			{

				String[] parts = line.split(" ");
				

				Vertex v1 = new Vertex();
				v1.setId(parts[0]);

				Vertex v2 = new Vertex();
				v2.setId(parts[1]);
				if(tweets.containsVertex(v1)==false)
				{
					tweets.addVertex(v1);	
				}
				
				tweets.addVertex(v2);
				if ((v1.getId() != null) && (v2.getId() != null)
						&& (v1.getId().equals(v2.getId()) == false)) {
					if ((tweets.containsEdge(v1, v2)) == false
							|| (tweets.containsEdge(v2, v1) == false)) {

						tweets.addEdge(v1, v2);
					}
				}

			}
			br2.close();

//			File file2 = new File("maxdegree_nodes_gre5_less10.txt");
//			PrintStream printStream = new PrintStream(new FileOutputStream(
//					file2));
//			System.setOut(printStream);
//
//			Set<Vertex> vertices = tweets.vertexSet();
//			// List<String> degnodes = new ArrayList<String>();
//			for (Vertex v : vertices) 
//			{
//				if (tweets.edgesOf(v).size() < 10 && tweets.edgesOf(v).size()>5) 
//				{
//					System.out.println(v.getId() + " "
//							+ tweets.edgesOf(v).size());
//
//				}
//
//			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println(tweets.edgeSet());

		}

		 Writer writer = new FileWriter("tweets_final.graphml");
		 GraphMLExporter<Vertex, DefaultEdge> exporter = new
		 GraphMLExporter<Vertex, DefaultEdge>(
		 new IntegerNameProvider<Vertex>(),
		 new VertexStatusProvider<Vertex>(),
		 new IntegerEdgeNameProvider<DefaultEdge>(), null);
		
		 try {
		 exporter.export(writer, tweets);
		 } catch (TransformerConfigurationException | SAXException e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }
	}
	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}
}
