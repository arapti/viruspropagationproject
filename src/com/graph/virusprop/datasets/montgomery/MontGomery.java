package com.graph.virusprop.datasets.montgomery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.ext.*;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;
import com.graph.virusprop.jgraph.VertexStatusProvider;


public class MontGomery {

	public static void main(String[] args) throws TransformerConfigurationException, SAXException {
		String file = "socnet_mont_va.txt";

		UndirectedGraph<Vertex, DefaultEdge> montGomery = createStringGraph();
		
		try {
		
			int times = 0;
			int timeval=0;
			double heal_rate1=0.0;
			double heal_rate2=0.0;
			double inf_rate1=0.0;
			double inf_rate2=0.0;
			

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			/*System.out.print("Enter the start range for node infection");
			min = Integer.parseInt(br.readLine());
			System.out.print("Enter the end range for node infection");
			max = Integer.parseInt(br.readLine());
			*/
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());
			
			
			System.out.print("Enter healing rate for profile 0 :");
			String input2 = br.readLine();
			heal_rate1 = Double.parseDouble(input2);
			
			System.out.print("Enter healing rate for profile 1 :");
			String input3 = br.readLine();
			heal_rate2 = Double.parseDouble(input3);
			
			System.out.print("Enter infection rate for profile 0 :");
			String input4 = br.readLine();
			inf_rate1 = Double.parseDouble(input4);
			
			System.out.print("Enter infection rate for profile 1 :");
			String input5 = br.readLine();
			inf_rate2 = Double.parseDouble(input5);
			
			br.close();
/*=============================================graph creation=======================================================*/			
			BufferedReader br2 = new BufferedReader(new FileReader(file));

			String line = null;
			
			int count=0;//number of nodes
			while ((line = br2.readLine()) != null) {

				String[] parts = line.split(" ");
				
				
				Vertex v1=new Vertex();
				v1.setId(parts[0]);
				v1.setActivity(Integer.parseInt(parts[1]));
								
				Vertex v2=new Vertex();
				v2.setId(parts[2]);
				v2.setActivity(Integer.parseInt(parts[3]));

				montGomery.addVertex(v1);
				montGomery.addVertex(v2);
				montGomery.addEdge(v1,v2);
				count++;
				if((count%1000)==0){
					System.out.println("Count is "+count);
					System.out.println("Vertices "+montGomery.vertexSet().size());
					System.out.println("Edges "+montGomery.edgeSet().size());
				}
				
			}
			br2.close();
			
			
			
			
			Set<Vertex> vertices2=montGomery.vertexSet();
			int max_deg = 0;
			for(Vertex v:vertices2){
				if(max_deg < montGomery.degreeOf(v))
					max_deg= montGomery.degreeOf(v);
				
			}
			System.out.println("Max degree is" +max_deg);
			
			//Set<Vertex> initialVertices=montGomery.vertexSet();
			int i = 0;
			int init_infect=0;
			for(Vertex v:vertices2){
				i++;
				
						
				
				if(i<vertices2.size()/2){
					if(montGomery.edgesOf(v).size()>125){
						v.setStatus(1);
						init_infect++;
					}else{
						v.setStatus(0);
					}
					v.setProfile(0);
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
				}
				else{
					if(montGomery.edgesOf(v).size()>125){
						v.setStatus(1);
						init_infect++;
					}else{
						v.setStatus(0);
					}
					v.setProfile(1);
					v.setHeal_rate(heal_rate2);
					v.setInf_rate(inf_rate2);
				}
				
				/*if((i<min)||(i>max)){//we choose the range of nodes that will get infected
					v.setStatus(0);
				}
				else{
					v.setStatus(1);
				}*/
			}
			
			
			
			
		/*	for(Vertex v:vertices2){
				i++;
				if(i<vertices2.size()/2){
					v.setProfile(0);
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
				}
				else{
					v.setProfile(1);
					v.setHeal_rate(heal_rate2);
					v.setInf_rate(inf_rate2);
				}
				
				if((i<min)||(i>max)){//we choose the range of nodes that will get infected
					v.setStatus(0);
				}
				else{
					v.setStatus(1);
				}
			}
			
			*/
			//System.out.println(montGomery.toString());
			
			Writer writer = new FileWriter("montgomery_init.graphml");
			GraphMLExporter<Vertex,DefaultEdge> exporter = 
				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
				    		new VertexStatusProvider<Vertex>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>()); 
						   
				    exporter.export(writer, montGomery);	  

			File file2 = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(file2));
			System.setOut(printStream);
			System.out.println("Initially infected: "+init_infect);
			
/*************************************** infection process *****************************************/
			Set<Vertex> vertices=montGomery.vertexSet();//select in set the nodes of the graph
			
			
			while (times <= timeval) {
				
				for(Vertex v:vertices){
					v.setNew_status(0);
					v.setDelta(0);
				}
				
				
				//for (int i = 0; i < N; i++) {
				for(Vertex v:vertices){
					
					
					
					if (v.getStatus() == 1) { // infected node

						double dice = Math.random();

				
						if((dice<=v.getHeal_rate())){
							
							v.setDelta(1);
							
						}
						
						
						//we collect the neighbors for each vertex
						for(DefaultEdge e:montGomery.edgesOf(v)){
							
						       Vertex vn=montGomery.getEdgeTarget(e);
						       if(vn==v){
						    	   vn=montGomery.getEdgeSource(e);
						       }
						       
						       dice = Math.random();
						      if(dice<=vn.getInf_rate()){
								vn.setNew_status(1);
						       }
						       
						}
					}//end of if
					
				}//end of for set iteration
				
				for(Vertex v:vertices){
					if((v.getStatus()==0)&&(v.getNew_status()==1)){
						v.setStatus(1);
						//System.out.println("Node "+v.getId()+" got infected");
					}
					
					else if((v.getStatus()==1)&&(v.getNew_status()==0)&&(v.getDelta()==1)){
						v.setStatus(0);
						//System.out.println("Node "+v.getId()+" got well");
					}
				}
					
				
						
				int sum=0;
				int	infect_a=0;
				int	infect_b=0;
				int infect_0=0; //home
				int infect_1=0; //work
				int infect_2=0; //shop
				int infect_3=0; //visit
				int infect_4=0; //social/recreation
				int infect_5=0; //other
				
				
				for(Vertex v:vertices){		
					if((v.getStatus()==1)&&(v.getProfile()==0)){
						infect_a++;
						if(v.getActivity()==0){
							infect_0++;
						}
						if(v.getActivity()==1){
							infect_1++;
						}
						if(v.getActivity()==2){
							infect_2++;
						}
						if(v.getActivity()==3){
							infect_3++;
						}
						if(v.getActivity()==4){
							infect_4++;
						}
						if(v.getActivity()==5){
							infect_5++;
						}
						
					}
					if((v.getStatus()==1)&&(v.getProfile()==1)){
						infect_b++;
						if(v.getActivity()==0){
							infect_0++;
						}
						if(v.getActivity()==1){
							infect_1++;
						}
						if(v.getActivity()==2){
							infect_2++;
						}
						if(v.getActivity()==3){
							infect_3++;
						}
						if(v.getActivity()==4){
							infect_4++;
						}
						if(v.getActivity()==5){
							infect_5++;
						}
												
						
					}
					
				}
				sum =sum+infect_a+infect_b;
			
				

				System.out.println("\n");
				System.out.println("Round: "+times);
				System.out.println("the amount of infected nodes is " + sum);
				
					System.out.println("profile 0:" + infect_a);
					System.out.println("profile 1:" + infect_b);
					System.out.println("Activity 0 - Home:" + infect_0);
					System.out.println("Activity 1 - Work:" + infect_1);
					System.out.println("Activity 2 - Shop:" + infect_2);
					System.out.println("Activity 3 - Visit:" + infect_3);
					System.out.println("Activity 4 - Social/Recreation:" + infect_4);
					System.out.println("Activity 5 - Other:" + infect_5);
				
				times++;
			}// end of while
			
			Writer writer2 = new FileWriter("montgomery_fin.graphml");
			GraphMLExporter<Vertex,DefaultEdge> exporter2 = 
				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
				    		new VertexStatusProvider<Vertex>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>()); 
						   
				    exporter2.export(writer2, montGomery);	  

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}

}
