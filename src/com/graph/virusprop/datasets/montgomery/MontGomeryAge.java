package com.graph.virusprop.datasets.montgomery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.ext.*;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;
import com.graph.virusprop.jgraph.VertexAgeStatusProvider;


public class MontGomeryAge {

	public static void main(String[] args) throws TransformerConfigurationException, SAXException {
		String file = "montogomery_age_data.txt";

		UndirectedGraph<Vertex, DefaultEdge> montGomery = createStringGraph();
		
		try {
			
			int times = 0;
			int timeval=0;
			double heal_rate1=0.0;
			double heal_rate2=0.0;
			double heal_rate3=0.0;
			double heal_rate4=0.0;
			double heal_rate5=0.0;
			double inf_rate1=0.0;
			double inf_rate2=0.0;
			double inf_rate3=0.0;
			double inf_rate4=0.0;
			double inf_rate5=0.0;

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());
			
			System.out.print("Enter healing rate for profile  1-10 :");
			String input = br.readLine();
			heal_rate1 = Double.parseDouble(input);
			
			System.out.print("Enter healing rate for profile  10-18 :");
			String input2 = br.readLine();
			heal_rate2 = Double.parseDouble(input2);
			
			System.out.print("Enter healing rate for profile 18-30 :");
			String input3 = br.readLine();
			heal_rate3 = Double.parseDouble(input3);
			
			System.out.print("Enter healing rate for profile under 30-50 :");
			String input4 = br.readLine();
			heal_rate4 = Double.parseDouble(input4);
			
			System.out.print("Enter healing rate for profile 50+ :");
			String input5 = br.readLine();
			heal_rate5 = Double.parseDouble(input5);
			
			System.out.print("Enter infection rate for profile 1-10 :");
			String input6 = br.readLine();
			inf_rate1 = Double.parseDouble(input6);
			
			System.out.print("Enter infection rate for profile 10-18 :");
			String input7 = br.readLine();
			inf_rate2 = Double.parseDouble(input7);
			
			System.out.print("Enter infection rate for profile 18-30 :");
			String input8 = br.readLine();
			inf_rate3 = Double.parseDouble(input8);
			
			

			System.out.print("Enter infection rate for profile 30-50 :");
			String input9 = br.readLine();
			inf_rate4 = Double.parseDouble(input9);
			
			System.out.print("Enter infection rate for profile 50+ :");
			String input10= br.readLine();
			inf_rate5 = Double.parseDouble(input10);
			br.close();
/*=============================================graph creation=======================================================*/			
			BufferedReader br2 = new BufferedReader(new FileReader(file));

			String line = null;
			
			int count=0;//number of nodes
			while ((line = br2.readLine()) != null) {

				String[] parts = line.split("\t");
				
				
				Vertex v1=new Vertex();
				v1.setId(parts[0]);
				v1.setActivity(Integer.parseInt(parts[1]));
				v1.setAge(Integer.parseInt(parts[5]));
								
				Vertex v2=new Vertex();
				v2.setId(parts[2]);
				v2.setActivity(Integer.parseInt(parts[3]));
				v1.setAge(Integer.parseInt(parts[6]));

				montGomery.addVertex(v1);
				montGomery.addVertex(v2);
				montGomery.addEdge(v1,v2);
				count++;
				if((count%1000)==0){
					System.out.println("Count is "+count);
					System.out.println("Vertices "+montGomery.vertexSet().size());
					System.out.println("Edges "+montGomery.edgeSet().size());
				}
				
			}
			br2.close();
			
			
			
			
			Set<Vertex> vertices2=montGomery.vertexSet();
			int max_deg = 0;
			for(Vertex v:vertices2){
				if(max_deg < montGomery.degreeOf(v))
					max_deg= montGomery.degreeOf(v);
				
			}
			System.out.println("Max degree is" +max_deg);
			
			int i = 0;
			int init_infect=0;//initially infected counter
			int counter=0;
			int counter2=0;
			int counter3=0;
			int counter4=0;
			int counter5=0;
			for(Vertex v:vertices2){
				i++;
				
				
				if(v.getAge()<=10){// we infect nodes based on their age profile
					if(counter%20==0){
						v.setStatus(1); //infected
						init_infect++;
						
					}else{
						v.setStatus(0); //not infected
					}
					counter++;
					v.setProfile(1);
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
					
				}
				
				if((v.getAge()>10)&&(v.getAge()<18)){
					if(counter2%100==0){
						v.setStatus(1);
						init_infect++;
						
					}else{
						v.setStatus(0);//not infected
					}
						v.setProfile(2);
						v.setHeal_rate(heal_rate2);
						v.setInf_rate(inf_rate2);
						counter2++;
					
					}
				if((v.getAge()>=18)&&(v.getAge()<30)){
					if(counter3%100==0){
						v.setStatus(1);
						init_infect++;
						
					}else{
					v.setStatus(0);
					}
					v.setProfile(3);
					v.setHeal_rate(heal_rate3);
					v.setInf_rate(inf_rate3);
					counter3++;
				}
				if((v.getAge()>=30)&&(v.getAge()<50)){
					if(counter4%100==0){
						v.setStatus(1);
						init_infect++;
						
					}else{
				
					v.setStatus(0);
					}
					v.setProfile(4);
					v.setHeal_rate(heal_rate4);
					v.setInf_rate(inf_rate4);
					counter4++;
				}
				
				if(v.getAge()>=50){
					if(counter5%100==0){
						v.setStatus(1);
						init_infect++;
						
					}else{
					v.setStatus(0);
					}
					v.setProfile(5);
					v.setHeal_rate(heal_rate5);
					v.setInf_rate(inf_rate5);	
					counter5++;
				}
					
			}//for
			//produce .graphml file of initially infected nodes
			Writer writer = new FileWriter("montgomery_age__init.graphml");
			GraphMLExporter<Vertex,DefaultEdge> exporter = 
				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
				    		new VertexAgeStatusProvider<Vertex>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>()); 
						   
				    exporter.export(writer, montGomery);	  

			
			//change output to file in order to keep track of the process
			File file2 = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(file2));
			System.setOut(printStream);
			System.out.println("Initially infected: "+init_infect);
			
/*************************************** infection process *****************************************/
			Set<Vertex> vertices=montGomery.vertexSet();//select in set the nodes of the graph
			
			while (times <= timeval) {
				
				for(Vertex v:vertices){
					v.setNew_status(0);
					v.setDelta(0);
				}
				
				//for (int i = 0; i < N; i++) {
				for(Vertex v:vertices){
						
					if (v.getStatus() == 1) { // infected node

						double dice = Math.random();

						if((dice<=v.getHeal_rate())){
							v.setDelta(1);	
						}
						
						//we collect the neighbors for each vertex
						for(DefaultEdge e:montGomery.edgesOf(v)){
							
						       Vertex vn=montGomery.getEdgeTarget(e);
						       if(vn==v){
						    	   vn=montGomery.getEdgeSource(e);
						       }
						       
						       dice = Math.random();
						      if(dice<=vn.getInf_rate()){
								vn.setNew_status(1);
						       }
						       
						}
					}//end of if
					
				}//end of for set iteration
				
				for(Vertex v:vertices){
					if((v.getStatus()==0)&&(v.getNew_status()==1)){
						v.setStatus(1);
						//System.out.println("Node "+v.getId()+" got infected");
					}
					
					else if((v.getStatus()==1)&&(v.getNew_status()==0)&&(v.getDelta()==1)){
						v.setStatus(0);
						//System.out.println("Node "+v.getId()+" got well");
					}
				}
					
				
						
				int sum=0;
				int	infect_a=0;//1-10
				int	infect_b=0;//10-18
				int	infect_c=0;//18-30
				int	infect_d=0;//30-50
				int	infect_e=0;//50+
				
				
				//add infected nodes based on the profile
				for(Vertex v:vertices){		
					if((v.getStatus()==1)&&(v.getProfile()==1)){
						infect_a++;
					}
					if((v.getStatus()==1)&&(v.getProfile()==2)){
						infect_b++;
					}
					if((v.getStatus()==1)&&(v.getProfile()==3)){
						infect_c++;
					}
					if((v.getStatus()==1)&&(v.getProfile()==4)){
						infect_d++;
					}
					if((v.getStatus()==1)&&(v.getProfile()==5)){
						infect_e++;
					}
						
				}
							
				//final sum of infected nodes		
				sum =sum+infect_a+infect_b+infect_c+infect_d+infect_e;

				System.out.println("\n");
				System.out.println("Round: "+times);
				System.out.println("the amount of infected nodes is: " + sum);
				
					System.out.println("profile 1-> people 1-10 years old: " + infect_a);
					System.out.println("profile 2-> people 10-18 years old: " + infect_b);
					System.out.println("profile 3-> people 18-30 years old: " + infect_c);
					System.out.println("profile 4-> people 30-50 years old: " + infect_d);
					System.out.println("profile 5-> people 50+ years old: " + infect_e);
					
			
				
				times++;
			}// end of while
			
			//produce .graphml file of finally infected nodes
			Writer writer2 = new FileWriter("montgomery_age_fin.graphml");
			GraphMLExporter<Vertex,DefaultEdge> exporter2 = 
				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
				    		new VertexAgeStatusProvider<Vertex>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
				    		new IntegerEdgeNameProvider<DefaultEdge>()); 
						   
				    exporter2.export(writer2, montGomery);	  

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}

}
