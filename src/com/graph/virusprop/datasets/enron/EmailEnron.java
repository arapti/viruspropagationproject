package com.graph.virusprop.datasets.enron;

import it.uniroma1.dis.wsngroup.gexf4j.core.Edge;
import it.uniroma1.dis.wsngroup.gexf4j.core.EdgeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.Mode;
import it.uniroma1.dis.wsngroup.gexf4j.core.Node;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.Attribute;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeClass;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeList;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeValue;
import it.uniroma1.dis.wsngroup.gexf4j.core.dynamic.TimeFormat;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.data.AttributeListImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import javax.xml.transform.TransformerConfigurationException;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.xml.sax.SAXException;

import com.graph.virusprop.jgraph.Vertex;


public class EmailEnron {

	public static void main(String[] args) throws TransformerConfigurationException, SAXException {
		String file ="Email-Enron.txt";

		UndirectedGraph<Vertex, DefaultEdge> emailenron = createStringGraph();
		
		try {
		
			int times = 0;
			int timeval=0;
			double heal_rate1=0.0;
			double heal_rate2=0.0;
			double inf_rate1=0.0;
			double inf_rate2=0.0;
			

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			/*System.out.print("Enter the start range for node infection");
			min = Integer.parseInt(br.readLine());
			System.out.print("Enter the end range for node infection");
			max = Integer.parseInt(br.readLine());
			*/
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());
			
			
			System.out.print("Enter healing rate for profile 0 :");
			String input2 = br.readLine();
			heal_rate1 = Double.parseDouble(input2);
			
			System.out.print("Enter healing rate for profile 1 :");
			String input3 = br.readLine();
			heal_rate2 = Double.parseDouble(input3);
			
			System.out.print("Enter infection rate for profile 0 :");
			String input4 = br.readLine();
			inf_rate1 = Double.parseDouble(input4);
			
			System.out.print("Enter infection rate for profile 1 :");
			String input5 = br.readLine();
			inf_rate2 = Double.parseDouble(input5);
			
			br.close();
/*=============================================graph creation=======================================================*/			
			BufferedReader br2 = new BufferedReader(new FileReader(file));

			String line = null;
			
			Gexf gexf = new GexfImpl();
			Calendar date = Calendar.getInstance();
			int date1=1000;
			gexf.getMetadata()
				.setLastModified(date.getTime())
				.setCreator("Gephi.org")
				.setDescription("A Web network");
			String version=gexf.getVersion();
			System.out.println("Version is "+version);


			Graph graph = gexf.getGraph();
			graph.
			setDefaultEdgeType(EdgeType.UNDIRECTED)
			.setMode(Mode.DYNAMIC)
			.setTimeType(TimeFormat.INTEGER);
			graph.setStartValue(date1);
			graph.setEndValue(date1+timeval);
			
			//create list of static attributes for class=node
			AttributeList staticAttrList = new AttributeListImpl(AttributeClass.NODE);
			staticAttrList.setMode(Mode.STATIC);
			graph.getAttributeLists().add(staticAttrList);
			
			//Static Attributes
			//Attribute attId=staticAttrList.createAttribute("0", AttributeType.STRING, "Id");
			Attribute attProfile=staticAttrList.createAttribute("0", AttributeType.INTEGER, "Profile");
			
			//create list of dynamic attributes for class=node
			AttributeList dynamicAttrList = new AttributeListImpl(AttributeClass.NODE);
			dynamicAttrList.setMode(Mode.DYNAMIC);
			graph.getAttributeLists().add(dynamicAttrList);
			
			//Dynamic Attributes
			Attribute attStatus=dynamicAttrList.createAttribute("1", AttributeType.INTEGER, "Status");
			
			
			
			int count=0;//number of nodes
			while ((line = br2.readLine()) != null) {

				String[] parts = line.split("\t");
				//String[] parts = line.split(" ");
				
				Vertex v1=new Vertex();
				v1.setId(parts[0]);
				Node vertex1=graph.createNode(parts[0]);
				vertex1.setLabel(parts[0]);
								
				Vertex v2=new Vertex();
				v2.setId(parts[1]);
				Node vertex2=graph.createNode(parts[1]);
				vertex2.setLabel(parts[1]);

				emailenron.addVertex(v1);
				emailenron.addVertex(v2);
				emailenron.addEdge(v1,v2);
				Edge edge=vertex1.connectTo(vertex2);
				
				count++;
//				if((count%50000)==0){
//					System.out.println("Count is "+count);
//					System.out.println("Vetrices "+emailenron.vertexSet().size());
//					System.out.println("Edges "+emailenron.edgeSet().size());
//				}
				
			}
			br2.close();
			
			
			Set<Vertex> vertices2=emailenron.vertexSet();
//			int max_deg = 0;
//			for(Vertex v:vertices2){
//				if(max_deg < emailenron.degreeOf(v))
//					max_deg= emailenron.degreeOf(v);
//				
//			}
//			System.out.println("Max degree is" +max_deg);
//			
//			int min_deg=max_deg;
//			for(Vertex v:vertices2){
//				if(min_deg >emailenron.degreeOf(v))
//					min_deg= emailenron.degreeOf(v);
//				
//			}
//			System.out.println("Min degree is" +min_deg);
			
			int i = 0;
			int init_infect=0;
			for(Vertex v:vertices2){
				i++;
				Node vertex1=graph.getNode(v.getId());
				
				//date.set(Calendar.YEAR,1000 );
				
				if(i<vertices2.size()/2){
					if(emailenron.edgesOf(v).size()>36){
						v.setStatus(1);
						
						vertex1.getAttributeValues().addValue(attStatus, "1");
						
						
					
						init_infect++;
					}else{
						v.setStatus(0);
						
						vertex1.getAttributeValues().addValue(attStatus, "0");
						
					}
					v.setProfile(0);
					
					vertex1.getAttributeValues().addValue(attProfile, "0");
					
					v.setHeal_rate(heal_rate1);
					v.setInf_rate(inf_rate1);
				}
				else{
					if(emailenron.edgesOf(v).size()>36){
						v.setStatus(1);
						init_infect++;
						
						vertex1.getAttributeValues().addValue(attStatus, "1");
						
					}else{
						v.setStatus(0);
						vertex1.getAttributeValues().addValue(attStatus, "0");
						
					}
					v.setProfile(1);
					vertex1.getAttributeValues().addValue(attProfile, "1");
					v.setHeal_rate(heal_rate2);
					v.setInf_rate(inf_rate2);
				}
				
				
				AttributeValue attValue=vertex1.getAttributeValues().get(0);
				attValue.setStartValue(date1);
				attValue.setEndValue(date1+1);
			}
		
//			//produce .graphml file of initially infected nodes
//			Writer writer = new FileWriter("emailenron_init.graphml");
//			GraphMLExporter<Vertex,DefaultEdge> exporter = 
//				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
//				    		new VertexStatusProvider<Vertex>(), 
//				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
//				    		null); 
//						   
//				    exporter.export(writer, emailenron);

			File file2 = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(file2));
			System.setOut(printStream);
		System.out.println("Initially infected: "+init_infect);
			
		
			
		
/*************************************** infection process *****************************************/
		Set<Vertex> vertices=emailenron.vertexSet();//select in set the nodes of the graph
			
			int round=0;
			while (times <= timeval) {
				round=date1+times;
				for(Vertex v:vertices){
					v.setNew_status(0);
					v.setDelta(0);
				}
				
				
				//for (int i = 0; i < N; i++) {
				for(Vertex v:vertices){
					
					
					
					if (v.getStatus() == 1) { // infected node

						double dice = Math.random();

				
						if((dice<=v.getHeal_rate())){
							
							v.setDelta(1);
							
						}
						
						
						//we collect the neighbors for each vertex
						for(DefaultEdge e:emailenron.edgesOf(v)){
							
						       Vertex vn=emailenron.getEdgeTarget(e);
						       if(vn==v){
						    	   vn=emailenron.getEdgeSource(e);
						       }
						       
						       dice = Math.random();
						      if(dice<=vn.getInf_rate()){
								vn.setNew_status(1);
						       }
						       
						}
					}//end of if
					
				}//end of for set iteration
				
				for(Vertex v:vertices){
					
					if((v.getStatus()==0)&&(v.getNew_status()==1)){
						v.setStatus(1);
						
						//System.out.println("Node "+v.getId()+" got infected");
					}
					
					else if((v.getStatus()==1)&&(v.getNew_status()==0)&&(v.getDelta()==1)){
						v.setStatus(0);
						//System.out.println("Node "+v.getId()+" got well");
						
					}
					if((times%500)==0){
						Node vertex1=graph.getNode(v.getId());
						int attValListSize=vertex1.getAttributeValues().size();
						vertex1.getAttributeValues().addValue(attStatus, Integer.toString(v.getStatus()));
						AttributeValue attValue=vertex1.getAttributeValues().get(attValListSize-1);
						attValue.setStartValue(round);
						attValue.setEndValue(round+1);
					}
				}
					
				
						
				int sum=0;
				int	infect_0=0;
				int	infect_1=0;
				for(Vertex v:vertices){		
					if((v.getStatus()==1)&&(v.getProfile()==0)){
						infect_0++;
					}
					if((v.getStatus()==1)&&(v.getProfile()==1)){
						infect_1++;
					}
					
				}
				sum = infect_0+infect_1;		
				

				System.out.println("\n");
				System.out.println("Round: "+times);
				System.out.println("the amount of infected nodes is " + sum);
				
					System.out.println("profile 0:" + infect_0);
					System.out.println("profile 1:" + infect_1);
				
				times++;
			}// end of while
			
			List<Node> graphnodes=graph.getNodes();
			for(int k=0; k<graphnodes.size(); k++){
				Node vertex1=graphnodes.get(k);
				int attValListSize=vertex1.getAttributeValues().size();
				if(attValListSize!=0){
					vertex1.getAttributeValues().remove(attValListSize-1);
				}			
			}
			
			
			StaxGraphWriter graphWriter = new StaxGraphWriter();
			File f = new File("dynamic_graph_sample.gexf");
			Writer out;
			try {
				out =  new FileWriter(f, false);
				graphWriter.writeToStream(gexf, out, "UTF-8");
				System.out.println(f.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
			//produce .graphml file of finally infected nodes
//			Writer writer2 = new FileWriter("emailenron_fin.graphml");
//			GraphMLExporter<Vertex,DefaultEdge> exporter2 = 
//				    new GraphMLExporter<Vertex, DefaultEdge>(new IntegerNameProvider<Vertex>(),
//				    		new VertexStatusProvider<Vertex>(), 
//				    		new IntegerEdgeNameProvider<DefaultEdge>(), 
//				    		null); 
//						   
//				    exporter2.export(writer2, emailenron);

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static UndirectedGraph<Vertex, DefaultEdge> createStringGraph() {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(
				DefaultEdge.class);

		return g;
	}

}
