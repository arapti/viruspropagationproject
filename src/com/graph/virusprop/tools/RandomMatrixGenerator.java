package com.graph.virusprop.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class RandomMatrixGenerator {

	public static void main(String[] args) throws IOException {
		int N = 0;
		double s = 0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of nodes:");
		N = Integer.parseInt(br.readLine());

		int[][] randomMatrix = new int[N][N];

		System.out.print("Enter propability limit");
		double val = Double.parseDouble(br.readLine());

		// random graph creation
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				s = Math.random(); // coin flip

				// we limit the amount of edges using the propability limit
				if (s < val) {

					randomMatrix[i][j] = 1;
					randomMatrix[j][i] = 1;

				} else {
					randomMatrix[i][j] = 0;
					randomMatrix[j][i] = 0;
				}
			}

		}

		File file = new File("random.txt");
		PrintStream printStream = new PrintStream(new FileOutputStream(file));
		System.setOut(printStream);// this turns output from console to the
									// file specified above

		for (int i = 0; i < N; i++) {

			for (int j = 0; j < N; j++) {

				System.out.print(randomMatrix[i][j] + " ");
			}

			System.out.print("\n");

		}

	}

}
