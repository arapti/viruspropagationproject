package com.graph.virusprop.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class RangeDegreeMatrixGenerator {
	// class for matrix generation where each node has a degree limit
	public static void main(String[] args) throws IOException {
		int N = 0;
		double s = 0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of nodes:");
		N = Integer.parseInt(br.readLine());

		int[][] randomMatrix = new int[N][N];

		System.out.print("Enter propability limit");
		double val = Double.parseDouble(br.readLine());

		System.out.print("Enter minimum degree");
		int min_deg = Integer.parseInt(br.readLine());

		System.out.print("Enter maximum degree");
		int max_deg = Integer.parseInt(br.readLine());

		
		int nod_count = 0;
		int i = 0;
		int j = 0;
		// neighbor counter
		int[] count = new int[N];
		// first we compute the neighbors for node 0
		for (i = 0; i < N; i++) {
			count[i] = 0;
			for (j = 0; j < N; j++) {
				// initialization of vector
				if (i == j) {
					randomMatrix[i][j] = 0;
				} else {
					s = Math.random(); // coin flip
					
					// we limit the amount of edges using the propability limit
					if ((s <= val) && (count[i] < max_deg)
							&& (randomMatrix[i][j] != 1)
							&& (count[j] < max_deg)) {

						randomMatrix[i][j] = 1;
						randomMatrix[j][i] = 1;
						count[i]++; // increase number of neighbors for node i
						count[j]++;
						

					} else {
						randomMatrix[i][j] = 0; // the graph is symmetric so
												// both
												// cells have to be zero or one
												// at
												// once
						randomMatrix[j][i] = 0;
					}

				}
				// end of else
			}// end of for

		}

		File file = new File("random.txt");
		PrintStream printStream = new PrintStream(new FileOutputStream(file));
		System.setOut(printStream);// this turns output from console to the
									// file specified above
		

		while (nod_count < N) {
			// System.out.println("Node count in loop is "+nod_count);
			nod_count = 0;
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {

					s = Math.random();
					if ((s < val) && (count[i] < max_deg)
							&& (count[j] < max_deg) && (i != j)
							&& randomMatrix[i][j] != 1) {
						randomMatrix[i][j] = 1;
						randomMatrix[j][i] = 1;
						count[i]++;
						count[j]++;
					}

				}

			}// end of for

			for (i = 0; i < N; i++) {
				count[i] = 0;
				for (j = 0; j < N; j++) {
					if (randomMatrix[i][j] == 1) {

						count[i]++;
					}

				}

				if (count[i] >= min_deg) {// if the node has at least the
											// minimum degree
					nod_count++;
				}

			}

			// if all nodes have at least the minimum degree, nod_count will be
			// N (the amount of nodes) so the process can finish
			if (nod_count == N) {

				for (i = 0; i < N; i++) {

					for (j = 0; j < N; j++) {

						System.out.print(randomMatrix[i][j] + " ");
					}

					System.out.print("\n");

				}
				break;

			}

		}

	}

}
