package com.graph.virusprop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VirusPropagationProcess {

	public static void main(String[] args) throws IOException {

		int N = 0;
		int e = 0; // flag for graph process

		String matrix;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter case number: "); //the user enters the case number to run
		String input = br.readLine();
		e = Integer.parseInt(input); // #of profiles

		/************************************************************** clique creation **********************************************************/
		if (e == 1) {
			System.out.print("Enter number of nodes"); 
            N =Integer.parseInt( br.readLine());
			
			
			CliqueVirusPropagationProcess.main(N);

		}

		/************************************************************* weighted clique creation ***************************************************/
		if (e == 2) {
			
			System.out.print("Enter number of nodes"); 
            N =Integer.parseInt( br.readLine());
			WeightedCliqueVirusPropagationProcess.main(N);

		}

		/************************************************************** load matrix from file *******************************************************/
		if (e == 3) {

			System.out.print("Enter file name for matrix:"); // in the form :
																// graph.txt
			matrix = br.readLine();

			FileMatrixVirusPropagationProcess.main( matrix);
		}
		
		/************************************************************** powerlaw *******************************************************/
		if (e == 4) {

			System.out.print("Enter file name for matrix:"); // in the form :
																// graph.txt
			matrix = br.readLine();
			PowerlawVirusPropagationProcess.main(matrix);
		}
		
		

	}

}
