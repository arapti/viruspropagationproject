package com.graph.virusprop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class CliqueVirusPropagationProcess {

	public static void main(int N) throws IOException {
		int min = 0;
		int max = 0;
		int timeval = 0;
		int times=0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of profiles: ");
		String input = br.readLine();
		int z = Integer.parseInt(input); // #of profiles

		System.out.print("Enter the start range for node infection");
		min = Integer.parseInt(br.readLine());
		System.out.print("Enter the end range for node infection");
		max = Integer.parseInt(br.readLine());
		
		System.out.print("Enter time steps limit for  the simulation");
		timeval = Integer.parseInt(br.readLine());

		int[][] randomMatrix = new int[N][N]; //graph matrix initialization
		int[][][] profile = new int[z][N][N];
		double[] heal_v = new double[z]; // healing rate vector
		double[] inf_v = new double[z]; // infection rate vector

		// clique creation
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {

				if (i == j) {
					randomMatrix[i][j] = 0;

				} else
					randomMatrix[i][j] = 1;
			}

		}

		int x = N / z;// # of nodes in each profile

		// assigning nodes to profiles
		for (int i = 0; i < z; i++) {
			for (int j = (i * x); j < ((i + 1) * x); j++) {

				profile[i][j][j] = 1;

			}
		}

		// get healing rate from user for each profile
		for (int i = 0; i < z; i++) {
			BufferedReader br2 = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Enter healing rate for profile " + i + ":");
			String input2 = br2.readLine();
			heal_v[i] = Double.parseDouble(input2);

		}

		// assign healing rate
		double[][][] heal = new double[z][N][N];

		for (int i = 0; i < z; i++) {
			for (int j = i * x; j < (i + 1) * x; j++) {

				heal[i][j][j] = heal_v[i];
			}

		}

		// get infection rate from user for each profile
		for (int i = 0; i < z; i++) {
			BufferedReader br3 = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Enter infection rate for profile " + i + ":");
			String input3 = br3.readLine();
			inf_v[i] = Double.parseDouble(input3);

		}

		// assign infection rate
		double[][][] infect = new double[z][N][N];

		for (int i = 0; i < z; i++) {
			for (int j = i * x; j < (i + 1) * x; j++) {

				infect[i][j][j] = inf_v[i];
			}

		}

		int[] p = new int[N];// state vector

		int[] new_p = new int[N];
		int[] delta = new int[N];

		File file = new File("status.txt");
		PrintStream printStream = new PrintStream(new FileOutputStream(file));
		System.setOut(printStream);

		for (int i = min; i <= max; i++) {
			p[i] = 1; // state vector initialization

		}

		System.out.println("The state vector is ");
		for (int i = 0; i < N; i++) {
			System.out.print(p[i]);

		}
		System.out.println("\n");

		/*************************************** infection process clique *****************************************/
		

		while (times <=timeval) {

			for (int i = 0; i < N; i++) {

				new_p[i] = 0;
				delta[i] = 0;
			}

			for (int i = 0; i < N; i++) {
				if (p[i] == 0) {// not infected

				}
				if ((p[i] == 1)) { // infected node
					double dice = Math.random();

					for (int j = 0; j < z; j++) {

						if (((profile[j][i][i]) == 1) && (dice <= heal_v[j])) {//checks in which profile the node is
							delta[i] = 1;

						}

					}

					for (int j = 0; j < N; j++) {
						if ((randomMatrix[i][j] == 1)) {//we get all neighbors
							dice = Math.random();
							for (int k = 0; k < z; k++) {
								if (profile[k][j][j] == 1) {
									if (dice <= inf_v[k])
										new_p[j] = 1;

								}
							}

						}
					}

				}

			}

			for (int i = 0; i < N; i++) {
				if ((p[i] == 0) && (new_p[i] == 1)) {
					p[i] = 1;
				}

				if ((p[i] == 1) && (new_p[i] == 0) && (delta[i] == 1)) {
					p[i] = 0;
				}
			}

			int[] temp = new int[N];
			int sum = 0;
			for (int k = 0; k < z; k++) {
				for (int i = 0; i < N; i++) {
					if ((p[i] == 1) && (profile[k][i][i] == 1)) {
						temp[k]++;
					}
				}

				sum = sum + temp[k];

			}

			System.out.println("\n");
			System.out.println("Round: "+times);
			System.out.println("the amount of infected nodes is " + sum);
			for (int i = 0; i < z; i++) {
				System.out.println("profile " + i + ":" + temp[i]);

			}
			times++;

		}// end of while

	}

}
