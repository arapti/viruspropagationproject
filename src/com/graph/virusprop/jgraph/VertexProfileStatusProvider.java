package com.graph.virusprop.jgraph;

import org.jgrapht.ext.VertexNameProvider;

import com.graph.random.Vertex;

public class VertexProfileStatusProvider <V> implements VertexNameProvider <V> {

	@Override
	public String getVertexName(V vertex) {
		// TODO Auto-generated method stub
				String status=getStatus((Vertex) vertex);
				int profile = getProfile((Vertex) vertex);
				status=Integer.toString(profile).concat(","+status);
				
				return status;	
					
	}
	
	
	public String  getStatus(Vertex v) {
		return Integer.toString(v.getStatus());
	}
	
	
	// get Profile method of vertex
		public int getProfile(Vertex v) {
			return v.getProfile();
		}

}
