package com.graph.virusprop.jgraph;

import com.graph.virusprop.jgraph.Vertex;

public class Vertex {
	
	private String Id;
	private int profile;
	private double  inf_rate;
	private double heal_rate;
	private int status;
	private int delta;
	private int new_status;
	private int activity;
	private int age;
	
	
	
	
	public Vertex() {
		
	}
	public Vertex(String id, int profile, double inf_rate, double heal_rate,
			int status,int delta,int new_status, int activity, int age) {
		super();
		Id = id;
		this.profile = profile;
		this.inf_rate = inf_rate;
		this.heal_rate = heal_rate;
		this.status = status;
		this.delta=delta;
		this.new_status=new_status;
		this.activity=activity;
		this.age=age;
		
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public int getProfile() {
		return profile;
	}
	public void setProfile(int profile) {
		this.profile = profile;
	}
	public double getInf_rate() {
		return inf_rate;
	}
	public void setInf_rate(double inf_rate) {
		this.inf_rate = inf_rate;
	}
	public double getHeal_rate() {
		return heal_rate;
	}
	public void setHeal_rate(double heal_rate) {
		this.heal_rate = heal_rate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getDelta() {
		return delta;
	}
	public void setDelta(int delta) {
		this.delta = delta;
	}
	
	public int getNew_status() {
		return new_status;
	}
	public void setNew_status(int new_status) {
		this.new_status = new_status;
	}
	
	public int getActivity() {
		return activity;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	

		
	@Override
	public String toString() {
		return "Vertex [Id=" + Id + ", profile=" + profile + ", inf_rate="
				+ inf_rate + ", heal_rate=" + heal_rate + ", status=" + status+", delta="
				+delta+", new_status="+new_status+", activity="+activity+", age="+age
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		return true;
	}
	
	

	
	
}
