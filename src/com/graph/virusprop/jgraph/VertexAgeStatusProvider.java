package com.graph.virusprop.jgraph;

import org.jgrapht.ext.VertexNameProvider;

import com.graph.random.Vertex;

public class VertexAgeStatusProvider<V> implements VertexNameProvider<V> {
	//we create vertex provider as in label based on the status and age  of each vertex
	@Override
	public String getVertexName(V vertex) {
		// TODO Auto-generated method stub
		String status = getStatus((Vertex) vertex);

		if (status.equals("0")) {
			status = status.concat(",0");
		} else {
			int age = getAge((Vertex) vertex);// we group nodes according to
												// their ages to profiles 1-5 if
												// they are infected.This is
												// only for visualization
												// purposes.
			if (age < 10) {
				status = "1,".concat(status);
			}
			if ((age >= 10) && (age < 18)) {
				status = "2,".concat(status);
			}
			if ((age > 18) && (age < 30)) {
				status = "3,".concat(status);
			}
			if ((age >= 30) && (age < 50)) {
				status = "4,".concat(status);
			}
			if (age >= 50) {
				status = "5,".concat(status);
			}

		}
		return status;
	}

	// get Status method of vertex
	public String getStatus(Vertex v) {
		return Integer.toString(v.getStatus());
	}

	// get Age method of vertex
	public int getAge(Vertex v) {
		return v.getAge();
	}

}
