	package com.graph.virusprop;

	import java.io.BufferedReader;
	import java.io.File;
	import java.io.FileOutputStream;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.io.PrintStream;

import com.graph.random.MatrixLoader;

	public class PowerlawVirusPropagationProcess {

		public static void main( String matrix) throws IOException {
			
			int times = 0;
			int timeval=0;
			int N=0;

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter number of profiles: ");
			String input = br.readLine();
			int z = Integer.parseInt(input); // #of profiles

			int[][] randomMatrix = MatrixLoader.loadDense(matrix);
			N=randomMatrix.length;
			int max_deg=0;
			int [] deg=new int[N];
			for(int i=0; i<N; i++){
				deg[i]=0;
				for(int j=0; j<N; j++){
					if(randomMatrix[i][j]==1){
						deg[i]++;
						
					}
					if(max_deg < deg[i])
						max_deg= deg[i];
					
				}	
					
					
				}
				
				
				
			System.out.println("Max degree is" +max_deg);
			
			int min_deg=max_deg;
			for(int i=0; i<N; i++){
				deg[i]=0;
				for(int j=0; j<N; j++){
					if(randomMatrix[i][j]==1){
						deg[i]++;
						
					}
					if(min_deg > deg[i]){
						min_deg= deg[i];
					}
				}	
					
					
				}
				
			System.out.println("Min degree is" +min_deg);
			
			
			

		/*	System.out.print("Enter the start range for node infection");
			min = Integer.parseInt(br.readLine());
			System.out.print("Enter the end range for node infection");
			max = Integer.parseInt(br.readLine());
		*/	
			System.out.print("Enter time steps limit for  the simulation");
			timeval = Integer.parseInt(br.readLine());


			int[][][] profile = new int[z][N][N];
			double[] heal_v = new double[z]; // healing rate vector
			double[] inf_v = new double[z]; // infection rate vector

			int x = N / z;// # of nodes in each profile

			// assigning nodes to profiles
			for (int i = 0; i < z; i++) {
				for (int j = (i * x); j < ((i + 1) * x); j++) {

					profile[i][j][j] = 1;

				}
			}

			// get healing rate from user for each profile
			for (int i = 0; i < z; i++) {
				BufferedReader br2 = new BufferedReader(new InputStreamReader(
						System.in));
				System.out.print("Enter healing rate for profile " + i + ":");
				String input2 = br2.readLine();
				heal_v[i] = Double.parseDouble(input2);

			}

			// assign healing rate
			double[][][] heal = new double[z][N][N];

			for (int i = 0; i < z; i++) {
				for (int j = i * x; j < (i + 1) * x; j++) {

					heal[i][j][j] = heal_v[i];
				}

			}

			// get infection rate from user for each profile
			for (int i = 0; i < z; i++) {
				BufferedReader br3 = new BufferedReader(new InputStreamReader(
						System.in));
				System.out.print("Enter infection rate for profile " + i + ":");
				String input3 = br3.readLine();
				inf_v[i] = Double.parseDouble(input3);

			}

			// assign infection rate
			double[][][] infect = new double[z][N][N];

			for (int i = 0; i < z; i++) {
				for (int j = i * x; j < (i + 1) * x; j++) {

					infect[i][j][j] = inf_v[i];
				}

			}

			int[] p = new int[N];// state vector

			int[] new_p = new int[N];
			int[] delta = new int[N];
			BufferedReader br3 = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Choose way of infection 1:for nodes with small degree 2:for nodes with high degree:");
			String input7 = br3.readLine();
			int option = Integer.parseInt(input7);
			int infect_nodes=0;
			if(option==1){
				System.out.print("Choose degree limit to infect nodes");
				String input8=br3.readLine();
				int deg_limit=Integer.parseInt(input8);
				int counter=0;
				for(int i=0; i<N; i++){
					
					if(deg[i]<=deg_limit){
						if(counter%20==0){
						p[i]=1;
						infect_nodes++;
						
					}else p[i]=0;
						counter++;
					}
					
				}
				
				
				
				
			}
			if (option==2){
				System.out.print("Choose degree limit to infect nodes");
				String input8=br3.readLine();
				int deg_limit=Integer.parseInt(input8);
				
				for(int i=0; i<N; i++){
					if(deg[i]>=deg_limit){
						p[i]=1;
						infect_nodes++;
					}else p[i]=0;
					
				}
				
			}
			System.out.println("Initially infected nodes: "+infect_nodes);
			
			
//			for (int i = min; i <= max; i++) {
//				p[i] = 1; // state vector initialization
	//
//			}

			File file = new File("status.txt");
			PrintStream printStream = new PrintStream(new FileOutputStream(file));
			System.setOut(printStream);

			System.out.println("The state vector is ");
			for (int i = 0; i < N; i++) {
				System.out.print(p[i]);

			}
			System.out.println("\n");

			/*************************************** infection process file matrix *****************************************/
			

			while (times <= timeval) {

				for (int i = 0; i < N; i++) {

					new_p[i] = 0;
					delta[i] = 0;
				}

				for (int i = 0; i < N; i++) {
					if (p[i] == 0) { // not infected

					}
					if ((p[i] == 1)) { // infected node

						double dice = Math.random();

						for (int j = 0; j < z; j++) {

							if (((profile[j][i][i]) == 1) && (dice <= heal_v[j])) {
								delta[i] = 1;

							}

						}

						for (int j = 0; j < N; j++) {
							if ((randomMatrix[i][j] == 1)) {
								dice = Math.random();
								for (int k = 0; k < z; k++) {
									if (profile[k][j][j] == 1) {
										if (dice <= inf_v[k])
											new_p[j] = 1;

									}
								}

							}
						}

					}

				}

				for (int i = 0; i < N; i++) {
					if ((p[i] == 0) && (new_p[i] == 1)) {
						p[i] = 1;
					}

					if ((p[i] == 1) && (new_p[i] == 0) && (delta[i] == 1))
						p[i] = 0;

				}

				int[] temp = new int[N];
				int sum = 0;
				for (int k = 0; k < z; k++) {
					for (int i = 0; i < N; i++) {
						if ((p[i] == 1) && (profile[k][i][i] == 1)) {
							temp[k]++;
						}
					}

					sum = sum + temp[k];

				}

				System.out.println("\n");
				System.out.println("Round: "+times);
				System.out.println("the amount of infected nodes is " + sum);
				for (int i = 0; i < z; i++) {
					System.out.println("profile " + i + ":" + temp[i]);

				}
				times++;
			}// end of while

		}
	}


