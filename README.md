# README #

### What is this repository for? ###

* A Java project for generating arbitrary graphs or graphs of specific topology. In addition, the project provides a simulator for the SIS model where nodes have different infection propabilites and healing propabilities for various networks.
*Version 1.0

### How do I get set up? ###

*Simulator instructions:

We import the Virus PropagationProjectSource project in any Java platform -- Eclipse etc.

A)RandomGraph is the executable file for the simulation.
You choose case for every type of topology you want to use:
1:Clique ---- > Clique.java is executed
2:Clique with weights on edges----> WClique.java is executed
3:Graph from file ---->FMatrix.java is executed, the file name given in prompt should be "name.txt" where name is the name of the file you want to load.

B)For random matrix generation execute file MatrixGen.java --->the output file is random.txt in root file of the project

C)In order to load graphs from file put them in the root file of the project.
Graphs from file should be in form of adjacency matrices.


D)To produce random matrices where nodes have a specific degree in range (min,max) run RangeDegree.java


E)To simulate the EnronEmail network, run EmailEnron.java, the output produces 2 .graphml files that show the initially infected and finally infected nodes.
You can load them in Gephi to see the visualization. In this class we make use of JGraph Library for large networks.


F)To simulate the Montgomery network, run MontGomery.java, the output produces 2 .graphml files that show the initially infected and finally infected nodes.
You can load them in Gephi to see the visualization.In this class we make use of JGraph Library for large networks.


G)To simulate the Montgomery network with age data , run MontgomeryAge.java. The dataset that is used is montogomery_age_data.txt. The output produces 2 .graphml files that show the initially infected and finally infected nodes.
You can load them in Gephi to see the visualization. In this class we make use of JGraph Library for large networks.



The only library required is the standard JDK library for Java.

* Repo owner or admin